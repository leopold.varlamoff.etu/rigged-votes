# R5.A.13 - Économie durable et numérique
## Projet individuel : smart contract (rigged-votes)
### 1. Smart contract

Le smart contract respecte les différentes recommandations et exigences.

Il est possible d'enregistrer des propositions, de voter, et le gagnant est choisi automatiquement parmi les propositions ayant remporté le plus de voix.

### 2. DWYW

Mon système de vote est assez spécial car il ne respecte absolument pas le processus démocratique habituel.

En effet, au moment du vote, il est possible de dépenser de l'ether pour que notre voix pèse plus que les autres. Le vote fonctionne ainsi : chaque vote aura un poids de 1 de base, puis chaque ether dépensé au moment de la transaction ajoutera 1 vote à la proposition choisie.

Le montant est arbitraire et choisi dans un environnement de test, mais il pourrait bien sûr être modifié si l'on choisit de déployer ce système de vote assez douteux sur la blockchain.

En ce qui concerne le code, la fonction vote utilise le mot clé payable, qui permet à l'utilisateur de la fonction de déposer une somme d'ether dans le "solde" du contrat. Lors du lancement de la fonction tallyVotes qui compte les voix, l'ether déposé est transféré au détenteur du contrat.

### 3. DApp

Malheureusement j'ai eu trop de problèmes aved la Dapp (même en utilisant `create-web3` il était impossible de faire fonctionner l'application). J'avais de gros problèmes avec les dépendances du projet et dès que je règlais un problème, 10 autres apparaissaient. Je n'ai donc rien à vous proposer pour cette partie :(