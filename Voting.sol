// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {
    struct Voter {
        bool registered;
        bool hasVoted;
        uint256 votedProposalId;
    }

    struct Proposal {
        string details;
        uint256 voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint256 proposalId);
    event Voted(address voter, uint256 proposalId);

    WorkflowStatus private currentStep;
    mapping(address => Voter) private voters;
    mapping(address => bool) private voterProposal;
    Proposal[] private proposals;
    uint256 private winningProposalId;

    constructor() Ownable(msg.sender) {
        currentStep = WorkflowStatus.RegisteringVoters;
    }

    modifier correctVotingStep(WorkflowStatus requiredStatus) {
        require(currentStep == requiredStatus, "Forbidden at this step");
        _;
    }

    modifier inWhitelist() {
        require(voters[msg.sender].registered, "User not registered");
        _;
    }

    function addVoter(address voter) public correctVotingStep(WorkflowStatus.RegisteringVoters) onlyOwner {
        require(!voters[voter].registered, "Adress already registered");
        voters[voter].registered = true;
        emit VoterRegistered(voter);
    }

    function startProposalRegistration() public correctVotingStep(WorkflowStatus.RegisteringVoters) onlyOwner {
        currentStep = WorkflowStatus.ProposalsRegistrationStarted;
    }

    function registerProposal(string memory description) public correctVotingStep(WorkflowStatus.ProposalsRegistrationStarted) inWhitelist {
        require(!voterProposal[msg.sender], "One proposal per user");
        voterProposal[msg.sender] = true;
        proposals.push(Proposal(description, 0));
        emit ProposalRegistered(proposals.length - 1);
    }

    function endProposalRegistration() public correctVotingStep(WorkflowStatus.ProposalsRegistrationStarted) onlyOwner {
        require(proposals.length > 0, "Add one proposal before ending");
        currentStep = WorkflowStatus.ProposalsRegistrationEnded;
    }

    function startVotingSession() public correctVotingStep(WorkflowStatus.ProposalsRegistrationEnded) onlyOwner {
        currentStep = WorkflowStatus.VotingSessionStarted;
    }

    // DYWY : votes 
    function vote(uint proposalId) public correctVotingStep(WorkflowStatus.VotingSessionStarted) inWhitelist payable {
        require(!voters[msg.sender].hasVoted, "Already voted");
        require(proposalId < proposals.length, "ID invalid");
        uint votes = msg.value / 1 ether + 1;
        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = proposalId;
        proposals[proposalId].voteCount += votes;
        emit Voted(msg.sender, proposalId);
    }

    function endVotingSession() public correctVotingStep(WorkflowStatus.VotingSessionStarted) onlyOwner {
        currentStep = WorkflowStatus.VotingSessionEnded;
    }

    function tallyVotes() public correctVotingStep(WorkflowStatus.VotingSessionEnded) onlyOwner {
        uint winnerId = 0;
        for (uint i = 1; i < proposals.length; i++) {
            if (proposals[i].voteCount > proposals[winnerId].voteCount) {
                winnerId = i;
            }
        }
        winningProposalId = winnerId;
        currentStep = WorkflowStatus.VotesTallied;
        address payable ownerAddress = payable(owner());
        ownerAddress.transfer(address(this).balance);
    }

    function getCurrentStep() public view returns (WorkflowStatus) {
        return currentStep;
    }

    function getProposals() public view returns (Proposal[] memory) {
        require(currentStep != WorkflowStatus.RegisteringVoters, "Proposals not accessible");
        return proposals;
    }

    function getWinner() public view correctVotingStep(WorkflowStatus.VotesTallied) returns (uint) {
        return winningProposalId;
    }
}
